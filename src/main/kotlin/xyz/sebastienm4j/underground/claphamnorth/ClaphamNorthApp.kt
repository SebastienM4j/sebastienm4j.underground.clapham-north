package xyz.sebastienm4j.underground.claphamnorth

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.runApplication
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer

@SpringBootApplication
class ClaphamNorthApp : SpringBootServletInitializer()
{
    override fun configure(builder: SpringApplicationBuilder) : SpringApplicationBuilder
    {
        return builder.sources(ClaphamNorthApp::class.java)
    }
}

fun main(args: Array<String>)
{
	runApplication<ClaphamNorthApp>(*args)
}
