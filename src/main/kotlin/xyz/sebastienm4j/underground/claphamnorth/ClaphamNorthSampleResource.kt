package xyz.sebastienm4j.underground.claphamnorth

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v0/hello")
class ClaphamNorthSampleResource
{
    @GetMapping
    fun hello() = "Clapham North Project"
}
